
#include <QGuiApplication>

#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>

#include <QTimer>
#include <QGuiApplication>

#include <QQuickPaintedItem>
#include <QPainter>

/**
 *
 *
 * https://bjk5.com/post/44698559168/breaking-down-amazons-mega-dropdown
 */

class TriangleMouseFilter : public QQuickPaintedItem
{
    Q_OBJECT
    /**
     * The timeout in ms after which the filter is disabled and the current item is selected
     * regardless.
     *
     * The default is 300
     * Setting a negative value disables the timeout
     */
    Q_PROPERTY(int filterTimeOut MEMBER m_filterTimeout NOTIFY filterTimoutChanged)

    /**
     * The edge that we want to filter mouse actions towards.
     * i.e if we have a listview on the left with a submenu on the right, the value
     * will be Qt.RightEdge
     *
     * RTL configurations must be handled explicitly by the caller
     */
    Q_PROPERTY(Qt::Edge edge MEMBER m_edge NOTIFY edgeChanged)

public:
    TriangleMouseFilter(QQuickItem *parent = nullptr)
        : QQuickPaintedItem(parent)
    {
        setFiltersChildMouseEvents(true);

        connect(&m_resetTimer, &QTimer::timeout, this, [this]() {
            m_interceptionPos = QPointF();
            update();
        });
    };

    ~TriangleMouseFilter() {}


    bool childMouseEventFilter(QQuickItem *item, QEvent *event) override
    {
        switch (event->type()) {
            case QEvent::HoverLeave:
                m_interceptedHoverItem.clear();
                return false;
            case QEvent::HoverEnter:
            case QEvent::HoverMove:
            {
                QHoverEvent *he = static_cast<QHoverEvent *>(event);

                QPointF position = item->mapToItem(this, he->posF());

                if (filterContains (position)) {
                    if (event->type() == QEvent::HoverEnter) {
                        m_interceptedHoverItem = item;
                    }

                    if (m_filterTimeout > 0) {
                        m_resetTimer.start(m_filterTimeout);
                    }
                    event->accept();

                    return true;
                } else {
                    m_interceptionPos = position;
                    event->setAccepted(false);
                    update();

                    // if we are no longer inhibiting events and have previously intercepted a hover enter
                    // we manually send the hover enter to that item so it can update it's containsMouse
                    if (event->type() == QEvent::HoverMove && m_interceptedHoverItem ) {
                        QHoverEvent e = QHoverEvent(QEvent::HoverEnter, position, position);
                        mapToItem(m_interceptedHoverItem, position);
                        qApp->sendEvent(m_interceptedHoverItem, &e);
                        m_interceptedHoverItem.clear();
                    }
                    return false;
                }
            }
            default:
                return false;
        }
    }

    bool filterContains(const QPointF &p) const  {
        if ( m_interceptionPos.isNull()) {
            return false;
        }

        QPolygonF poly;
        poly << m_interceptionPos ;

        switch(m_edge) {
            case Qt::RightEdge:
                poly << QPointF(width()+1, height()) << QPointF(width()+1, 0);
                break;
            case Qt::TopEdge:
                poly << QPointF(0, -1) << QPointF(width(), -1);
                break;
            case Qt::LeftEdge:
                poly << QPointF(-1, 0) << QPointF(-1, height());
                break;
            case Qt::BottomEdge:
                poly << QPointF(0, height() + 1) << QPointF(width(), height() + 1);
        }

        return poly.containsPoint(p, Qt::OddEvenFill);
    }

    // kill this in the submission
    void paint(QPainter *painter) override {
        if ( m_interceptionPos.isNull()) {
            return;
        }

        QPolygonF poly;
        poly << m_interceptionPos ;

        switch(m_edge) {
            case Qt::RightEdge:
                poly << QPointF(width()+1, height()) << QPointF(width()+1, 0);
                break;
            case Qt::TopEdge:
                poly << QPointF(0, -1) << QPointF(width(), -1);
                break;
            case Qt::LeftEdge:
                poly << QPointF(-1, 0) << QPointF(-1, height());
                break;
            case Qt::BottomEdge:
                poly << QPointF(0, height() + 1) << QPointF(width(), height() + 1);
        }

        QColor color(Qt::red);
        color.setAlphaF(0.2);
        painter->setBrush(color);
        painter->drawPolygon(poly);
    }

Q_SIGNALS:
    void filterTimoutChanged();
    void edgeChanged();

private:
    QTimer m_resetTimer;
    QPointF m_interceptionPos;
    QPointer<QQuickItem> m_interceptedHoverItem;
    Qt::Edge m_edge = Qt::RightEdge;
    int m_filterTimeout = 300;
};



int main(int argc, char ** argv)
{
    QGuiApplication app(argc, argv);
    qmlRegisterType<TriangleMouseFilter>("net.dave", 1,0, "TriangleMouseFilter");

    QQuickView view;
    view.setSource(QUrl("qrc:view.qml"));
    view.show();

    return app.exec();
}

#include "main.moc"
