import QtQuick 2.0
import net.dave 1.0

Rectangle {
    id: root
    width: 800; height: 400

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    TriangleMouseFilter {
        anchors.left: parent.left
        width: 400
        height: 400
        edge: LayoutMirroring.enabled ? Qt.LeftEdge : Qt.RightEdge

        ListView {
            id: listView
            anchors.fill: parent

            model: 20
            delegate: Item {
                height: 50
                width: parent.width
                MouseArea {
                    property string dave: "idx" + model.index
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: listView.currentIndex = model.index
                }
                Text {
                    anchors.centerIn: parent
                    text: "Hello " + model.index
                }
            }
        }
    }


    Rectangle {
        color: "#ff000050"
        anchors.right: parent.right
        width: 400
        height: 400

        Text {
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 4
            text: "some top target"
            color: "white"
        }

        Text {
            anchors.centerIn: parent
            color: "white"
            text: "currentlyHovering " + listView.currentIndex
        }

        Text {
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.margins: 4
            text: "some bottom target"
            color: "white"

        }

    }

}
